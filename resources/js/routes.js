import Home from './components/Home.vue';
import Login from './components/auth/Login.vue';
import Register from './components/auth/Register.vue';

import QuestionnairesMain from './components/questionnaires/Main.vue';
import QuestionnairesList from './components/questionnaires/List.vue';
import CreateQuestionnaire from './components/questionnaires/Create.vue';
import Questionnaire from './components/questionnaires/Show.vue';

import QuestionsMain from './components/questions/Main.vue';
import QuestionsList from './components/questions/List.vue';
import CreateQuestions from './components/questions/Create.vue';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/questionnaires',
        component: QuestionnairesMain,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '/',
                component: QuestionnairesList
            },
            {
                path: 'create',
                component: CreateQuestionnaire
            },
            {
                path: ':id',
                component: Questionnaire
            },
            {
                path: ':id/edit/questions',
                component: QuestionsMain,
                children: [
                    {
                        path: '/',
                        component: QuestionsList
                    },
                    {
                        path: 'create',
                        component: CreateQuestions
                    }
                ]
            }
        ]
    }
];