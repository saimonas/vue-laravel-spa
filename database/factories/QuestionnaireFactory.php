<?php

use Faker\Generator as Faker;

$factory->define(App\Questionnaire::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 7),
        'title' => $faker->jobTitle,
        'description' => $faker->text,
    ];
});
