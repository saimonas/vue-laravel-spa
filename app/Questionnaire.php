<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'image'
    ];

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
